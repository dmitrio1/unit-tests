export const sum = (a, b) => a + b

export const sumCurry = a => b => a + b

export const cussWordsList = ['govno', 'zalupa', 'penis', 'her']
const splitter = /(\W+)/

export const cussWordsReplacer = str => {
  console.log(
    str
      .split(splitter)
      .map(el =>
        cussWordsList.includes(el.toLowerCase())
          ? Array(el.length)
              .fill('*')
              .join('')
          : el
      )
      .join('')
  )
  return str
    .split(splitter)
    .map(el =>
      cussWordsList.includes(el.toLowerCase())
        ? Array(el.length)
            .fill('*')
            .join('')
        : el
    )
    .join('')
}

describe('Cusswords filter', () => {
  // it('sum 2 numbers', () => {
  //   expect(sum(1, 2)).toEqual(3)
  // })
  // it('sum 2 numbers curry', () => {
  //   expect(sumCurry(1)(2)).toEqual(3)
  // })

  it('filters cusswords', () => {
    expect(cussWordsReplacer('govno cvetochek zalupa solnishko')).toEqual(
      '***** cvetochek ****** solnishko'
    )
  })

  it('dont crashes on empty strings', () => {
    expect(cussWordsReplacer('')).toEqual('')
  })

  it('ignores case', () => {
    expect(cussWordsReplacer('Govno gOvnO govno GOVNO')).toEqual(
      '***** ***** ***** *****'
    )
  })
})
